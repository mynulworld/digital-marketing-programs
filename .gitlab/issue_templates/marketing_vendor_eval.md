   
[DRI](https://about.gitlab.com/handbook/people-operations/directly-responsible-individuals/) to fill in as much as possible and assign yourself as owner initially. Please ensure the interested parties are part of the business needs evaluation.

* **Name of vendor**:
* **Evaluation DRI**:

### Evaluation process
* [ ]  Vendor pitch attended by:
* [ ]  Link or attach any documentation:
* [ ]  Buy-in from interested parties @ mention:
* [ ]  What campaign:
* [ ]  What audience:
* [ ]  ROI for GitLab:
* [ ]  If approved begin [Vendor contract approval process](https://about.gitlab.com/handbook/finance/procure-to-pay/)

### Business needs:
*  What marketing functions or services does this vendor perform?  What business problem does it solve?

### Users:
* What departments/people benefit from its use and is there a representative from each dept to evaluate?

### Status of existing vendor for service
* Do we currently have a vendor in use for this marketing function?

* If we do, why are we replacing it?

* What the current contractual status? Link to copy of contract:


### New vendor details
* What is the cost, use [funnel-o-matic](https://docs.google.com/spreadsheets/d/1RtcbV2babdpXWRcNCCe0XfkYXhKci9C980imHWBONDs/edit#gid=285896016) if needed:

* Can we pilot/poc/trial this vendor, or will we need to commit to a contract?

* How long is the contract?

* Is this budgeted? 

* Will this need to be integrated with any other tools (such as SFDC or Marketo, for example) 

* Will this require collaboration with MPMs, XDRs, or other team members?

* Primary features of this tool
1.  
1.  
1.   


