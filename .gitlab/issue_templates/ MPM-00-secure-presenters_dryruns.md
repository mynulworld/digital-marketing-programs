### 📝 [Epic Link >>](#)

## Purpose

This issue will track presenters and Q&A support as well as the prep work (presentation decks, dry runs) for an upcoming virtual event.

**To be completed by Marketing Program Manager:**


## :calendar\_spiral: Finalize date/time & Secure Webcast Team
* [ ] Confirmed Webcast Date Time: (Insert Webcast Date Time in the following format MM-DD-YYYY; Time local/Time UTC)
* [ ] Confirm with Speaker - MPM (internal webcast) | Webcast requestor (external webcast)
* [ ] Secure demo:(If applicable) - MPM
* [ ] Secure Q&A:(If applicable) - MPM
* [ ] Schedule dry run & webcast via Google Calendar - MPM

## :pencil: Prepare for Webcast (`1 week prior to webcast`)
* [ ] Ask presenters to fill in deck - MPM (internal webcast) | Webcast requestor (external webcast) (5 BD before Dry Run Date)
* [ ] Share webcast deck for feedback - Webcast requestor (2 BD before dry run date)
* [ ] Host webcast dry run - MPM

Please submit any questions about this template to the #marketing-programs slack channel.

/assign @aoetama

/label ~"Marketing Programs" ~"status:plan" ~"MPM - Secure presenters and schedule dry runs"