## A/B test template

### Primary considerations and questions to ask

1. **Problem:** Determine what business problem are we trying to solve with this change.
2. **Metric:** Choose your top metric to measure success. If you don't have a KPI in mind, think of it in terms of the business problem you want to solve. There can be more than one metric, but the test will be set-up to measure and score one primary metric.
3. **Location:** Choose a specific page or element that you would like to change to meet this business goal.
4. **Description of what you want to change:** Keep in mind that changing multiple elements may require multiple tests (try to limit testing only one element at a time) and making changes that require new images or elements not currently in our template may require additional time and help from other team members.
5. **Hypothesis:** Write out what you expect to happen based on the change you’re experimenting with a clear, data-informed rationale. 
6. **Duration:** How long should we run this test?

### Next steps

* Create this issue in the Digital Marketing Programs project and tag `@sdaily` and anyone else who may need to give input
* Make sure to add the `Conversion Rate Optimization (CRO)` label that will add this to the issue board into the backlog.