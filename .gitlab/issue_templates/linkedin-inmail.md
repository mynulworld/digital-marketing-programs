## Details needed for the LinkedIn Inmail


* Date Range for InMail Send: `Enter start and end date`
* Date of Event for InMail (if any): `Enter date of event`
* URL: `Enter URL to link people to`
* Geo-location(s) to target (if any): `Enter geo-location(s) to target (i.e. cities, states, countries, etc.)`
* InMail sender(s) profile URL: `Enter LinkedIn profile URL of sender(s). Will need to add to send list if not approved sender`
* Allow 1st degree connections?: `Yes or No`
* Industries: `Enter desired industries to target (we will match as closely as possible)`
* Company Size: `Enter desired company size to target`
* Company Name: `Enter company names to target employees from`
* Target GitLab LinkedIn Followers: `Yes or No`
* Fields of Study (if applicable): `Enter targeted field of study`
* Job Title: `Enter targeted job titles`
* Member Skills: `Enter skills of people you wish to target`
* Member Interests: `Enter topics of interest of the targeted audience`
* If you have a list of companies you want to target I need the list of companies with their email addresses in this format.
https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/example%20files/LinkedIn_Ads_Acct_Targeting_Template.csv

View LinkedIn targeting options for more detail:
https://www.linkedin.com/help/lms/answer/722/targeting-options-and-best-practices-for-linkedin-advertisements?lang=en

### Here are the LinkedIn requirements


* Subject (30 characters): `Enter subject line`
* Message (Under 500 characters. Make sure date, time, location are listed.): `Enter InMail message you want to send`
* Image: `Include a 300x250 image to use with your InMail.`
* CTA Text (i.e. Register, Sign Up, Download, etc).: `Enter CTA text to use`

**Note**: Formatting options like bulleting, italics, bolding, and underlining are available, although we recommend keeping the formatting simple and conversational since you're in a conversation space.

/assign @mnguyen4 @lbanks