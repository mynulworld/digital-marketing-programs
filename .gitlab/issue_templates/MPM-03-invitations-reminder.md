### 📝 [Epic Link >>](#)


## WIP email screenshot
**Sender: GitLab, info@**  
**Subject: fill in when final**

*copy paste image of most recent email here - when final, change the title of this section to "Final"*

## Purpose

This issue will track the identification for our target segment (should be included in prior reasoning for landing page creation), invitation and reminder emails for events. Adherence to schedules by supporting teams (field marketing and content) are required to hit deadlines for promotion in advance of event.

**Below to be completed by Marketing Program Manager with support of Field Marketing and Content Marketing.**

**The first email invitation will take place 25 business days prior to the event start date. Invitation two will be sent 16 business days prior to the event. The reminder email will be sent the day prior to the event.**

### [Copy Document >>](https://docs.google.com/document/u/1/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit) *make a copy of this template and provide requested copy*

**PLANNED INVITE 1 DATE:** `x` (Write copy by: `x`)  
**PLANNED INVITE 2 DATE:** `X` (Write copy by: `x`)  
**PLANNED REMINDER DATE:** `X` (Write copy by: `x`)  

## 🎯 Identify target segment

* [ ]  Provide suggested filters and required capacity of event (in copy doc) - campaign owner
* [ ]  Develop target list - MPM (with help of MOps)
* [ ]  Review target list on call or async if possible - MPM + campaign owner

## 🗓 Add invitation email to [email marketing calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_bpjvmm7ertrrhmms3r7ojjrku0%40group.calendar.google.com&ctz=America%2FLos_Angeles)
* [ ]  Invitation email 1
* [ ]  Invitation email 2
* [ ]  Reminder email

## 📧 Create Invitation 1 Email

* [ ]  Write copy for email - campaign owner
* [ ]  Provide edits in Copy Document - MPM
* [ ]  Set up email in Marketo and edit copy as needed - MPM
* [ ]  Test and approve email - MPM & campaign owner
* [ ]  Confirm in issue comments that email is set to send - MPM
* [ ]  Confirm in issue comments that email was sent out - MPM

## 📧 Create Invitation 2 Email

* [ ]  Write copy for email - campaign owner
* [ ]  Provide edits in Copy Document - MPM
* [ ]  Set up email in Marketo and edit copy as needed - MPM
* [ ]  Test and approve email - MPM & campaign owner
* [ ]  Confirm in issue comments that email is set to send - MPM
* [ ]  Confirm in issue comments that email was sent out - MPM

## 🔔 Create Reminder Email

* [ ]  Write copy for email - MPM (campaign owner to clarify ahead of time if agenda is required for the reminder)
* [ ]  Set up email in Marketo - MPM
* [ ]  Test and approve email - MPM & campaign owner
* [ ]  Confirm in issue comments that email is set to send - MPM
* [ ]  Confirm in issue comments that email was sent out - MPM

Please submit any questions about this template to the #marketing-programs slack channel.

/label ~"Marketing Programs" ~"status:wip" ~"MPM - Segmentation & Invitation"