### 📝 [Epic Link >>](#)

## Purpose

This issue will track the creation of campaign in Marketo/Salesforce and link the two for consistency between systems. Finance tags are requested from AP.

**To be completed by Marketing Program Manager:**

## 🏷 Create finance tag
[Link to list of tag requests >>](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=1741754421)
**Tag to use:** `MPM list here`

## 💙 Create Salesforce & Marketo Campaign(s)
  * [ ] [main salesforce campaign](https://na34.salesforce.com/701?fcf=00B61000004NY3B)
  * [ ] [main marketo program](https://app-ab13.marketo.com/#MF1441A1)
  * [ ] [additional salesforce campaign](https://na34.salesforce.com/701?fcf=00B61000004NY3B)
  * [ ] [additional marketo program](https://app-ab13.marketo.com/#MF1441A1)

## 🔗 Link MKTO <> SFDC
  * [ ] main campaign
  * [ ] additional campaign

## :green\_heart: Create Zoom Program(s) (only for GitLab Hosted Webcasts)
  * [ ] [Zoom program](https://zoom.us/webinar/list)

## 🔗 Link MKTO <> Zoom (only for GitLab Hosted Webcasts)
 * [ ]  Update zoom webinar id in Marketo token
 * [ ]  Set up integration in zoom to send registration, attendees, poll , and Q&A information to marketo
 
Please submit any questions about this template to the #marketing-programs slack channel.

/label ~"Marketing Programs" ~"status:wip" ~"MPM - Facilitate Tracking" ~"MktgOPS - FYI"