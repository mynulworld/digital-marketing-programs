If you are requesting digital marketing support for a campaign, event, webcast, new whitepaper, etc, please fill out the details below.

## Content

* GitLab URL: `Enter URL to drive traffic to`

* Purpose of campaign: `Enter in the reasoning for this campaign`

* Campaign Goal: `Enter tangible goal for this campaign (i.e. registrations, downloads, etc).`

* Camapign Start and End Dates: `Enter start and end date`

* Budget for Digital Marketing Campaign: `Enter in budget for campaign`

* Creative Asset: `Enter URL to GitLab creative asset to use`
     Platform creative requirements:
          - Facebook: `1200x628 pixels`
          - LinkedIn: `1200x628 pixels`
          - Twitter: `800x418 pixels`

## Targeting

* Geo-location(s) to target (if any): `Enter geo-location(s) to target (i.e. cities, states, countries, etc.)`

* Industries: `Enter desired industries to target (we will match as closely as possible)`

* Fields of Study (if applicable): `Enter targeted field of study`

* Audience Job Title: `Enter targeted job titles`

* Audience Skills: `Enter skills of people you wish to target`

* Audience Interests: `Enter topics of interest of the targeted audience`

* Salesforce Campaign (if applicable): `Enter URL to Salesforce campaign`


## Digital Marketing Program to complete**

Paid channels to run campaign

- [ ] Search: 

- [ ] Social: 

- [ ] Demand Gen Sponsorships: 

- [ ] Other (please describe): 

## Digital Marketing team actions

- [ ] Tag events, but first determine what events we need to track based on primary CTA
- [ ] Set-up a/b test [or delete this if there is none]
- [ ] Create UTM codes and share with team


If gated assets are needed, please link to these requests  as well as any other related issues.

/assign @mnguyen4 @lbanks

/label ~Digital Marketing Programs