# GATED CONTENT DETAILS

❌ Note: No gate content page should ever be pushed live without the inclusion and approval by a Marketing Program manager.

* **Content Owner:** 
* **Official Content Name:** 
* **Link to Content:** (attach if in PDF form)
* **Validity (for analyst reports):** (start date - end date)
* **Budgeted Cost (if relevant):** `requester fill in`
* **Reprint Rights (for analyst reports):** [example - Forrester Reprint Rights](https://www.forrester.com/staticassets/marketing/about/Forrester_Reprint-Client-Guide.pdf)
* **Citation Policy (for analyst reports):** [example - Forrester Citations Policy](https://www.forrester.com/marketing/policies/citations-policy.html)
* **Pushed Live Date:** @jgragnola to fill in when page and tracking live

### [Landing Page Copy Document >>](https://docs.google.com/document/d/1xHnLKPCaXrpEe1ccRh_7-IqgNbAlzQsZVc-wr1W4ng8/edit#)

# ACTION ITEMS

## Facilitate Tracking of Campaigns in Systems

* [ ] Create Salesforce Campaign @jgragnola ([tbd link](https://na34.salesforce.com/701?fcf=00B61000004NY3B))
* [ ] Create Marketo Program @jgragnola ([tbd link](https://app-ab13.marketo.com/#MF1521A1))
* [ ] Link MKTO <> SFDC @jgragnola
* [ ] Create Finance Tags (if relevant) @jgragnola (`MPM request from AP`)

## Create Confirmation Email

* [ ]  Write copy for email - @jgragnola
* [ ]  Set up email in Marketo - @jgragnola
* [ ]  Test email - @jgragnola

## Host Asset
* [ ]  Upload PDF to `/resources/downloads/` OR upload video to youtube as *unlisted* type - MPM
* [ ]  Test hosted asset for all hyperlinks and layout - MPM and requester

## Create Content Request Landing Page
* [ ]  Edit flows for each campaign in Marketo - MPM
* [ ]  Write copy for landing page - requester
* [ ]  Check character limits in [tracking doc](https://docs.google.com/spreadsheets/u/1/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=868164112) - requester (insert due date)
* [ ]  Provide edits to copy in Copy Document - MPM
* [ ]  Create page on www-gitlab-com - MPM
* [ ]  Test pipeline-approved page - MPM
* [ ]  Provide approved page in review app to requester, if analyst approval required - MPM

## Analyst Review (for reports)
* [ ]  Request review from analyst (if relevant) - requester
* [ ]  COMMENT IN ISSUE WITH SCREENSHOT OF ANALYST APPROVAL - requester

## Test and Push Live
* [ ]  Notify MPM of approval - requester
* [ ]  Merge MR - MPM
* [ ]  Test fully functioning page live - MPM
* [ ]  Notify requester in issue comments that page is live and traffic can be pushed there - MPM

Questions refer to [handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/)

/assign @jgragnola 

/label ~"Marketing Campaign" ~"Marketing Programs" ~"MktgOPS \- FYI" ~"MPM - Facilitate Tracking"

/epic <&epic &#155; group&epic &#155; https://gitlab.com/groups/gitlab-com/marketing/-/epics/155>