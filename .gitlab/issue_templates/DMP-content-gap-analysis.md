## Goals :goal: 
1. Improve SEO results
2. Increase audience engagement
3. Improve conversion rate

## Resources :tools: 


## Workflow

### Identifying Customer Questions :question: 

1. What questions are they being asked? https://gitlab.my.salesforce.com/00O4M000004FZBI
2. Slack search: `in:#questions prospect` (**note:** 90-day archive)
3. What problems are prospects facing?
4. Is there information prospects can’t find?

### Select 3-5 top non-branded keyword targets :dart:

1.  Agile Delivery 
2.  Application Security 
3.  Cloud Native
4.  Continuous Integration 
5.  DevOps
6.  Kubernetes 
7.  Microservices
8.  Source Control Management
9.  Value Stream Management

### Identifying Top Performing Content :chart_with_upwards_trend:

- [ ] Pull top queries from Google Search Console by keyword
- [ ] Pull top pages by query from Google Search Console by keyword
- [ ] Export questions from [Answerthepublic.com](https://answerthepublic.com/) (only 3 free searches allowed per day)
- [ ] Pull suggested keywords from Keyword Explorer (keyword lists) in Moz by keyword
- [ ] Pull high traffic pages (at least 1K pageviews) in Google Analytics filtered by keyword
*  filter: `Include page containing "keyword"`
*  filter: `Include page about.gitlab.com`
*  filter: `Include page docs.gitlab.com`
*  search docs.gitlab.com to ensure no other content
*  search about.gitlab.com to ensure no other content
*  ensure no data sampling occurring
- [ ] Create new column for type and separate page by content type (e.g. docs, product, blog, etc.)
- [ ] Add summary tab and pull all top pages for keyword by content type/channel (include top queries)
- [ ] Create Hotjar heatmap for top pages to identify user intent
- [ ] Map top pages and query by lifecycle stage (TOFU, MOFU, BOFU)
- [ ] Document findings in issue by keyword/topic
