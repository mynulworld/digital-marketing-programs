### 📝 [Epic Link >>](#)

## Purpose

This issue will track the creation of the landing page, confirmation email, calendar invitation, and automation for a campaign. Adherence to schedules by supporting teams are required to hit deadlines for promotion of the campaign.

**Below to be completed by Marketing Program Manager with support of campaign owner (i.e. Field Marketer):**

### [Copy Document >>](https://docs.google.com/document/u/1/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit) *make a copy of this template and provide requested copy*

## 🎨 Create issue for design assets (if relevant)
* [ ] Open design asset request issue in Corp Marketing project and assign to Luke Babb.

## 💜 Update tokens and flows in Marketo
* [ ]  Update copy for "save the date" calendar event in Marketo token
* [ ]  Update event details in all Marekto tokens (name, dates, location, link to epic, etc,)

## 📧 Create and test confirmation email in Marketo

* [ ]  Write copy for confirmation email
* [ ]  Set up email in Marketo
* [ ]  Test email

## 🛎️ Create and test lead alert email in Marketo (if relevant)

* [ ]  Review email to include additional details if needed - Marketo tokens should simplify this step
* [ ]  Test email with jgragnola+michael@gitlab.com as a test lead

## 💻 Create and test registration/meeting request landing page

* [ ]  Edit flows for each campaign - MPM
* [ ]  Write copy for landing page - campaign owner
* [ ]  Provide edits in Copy Document linked above -  MPM
* [ ]  Page creation in www-gitlab-com or Marketo - MPM
* [ ]  Review pipeline-approved page - MPM & campaign owner
* [ ]  Merge and activate automation in Marketo - MPM
* [ ]  Add to `/events/` page in www-gitlab-com - MPM
* [ ]  Test fully functioning page live - MPM
* [ ]  Notify event organizer when ready to be promoted - MPM

Please submit any questions about this template to the #marketing-programs slack channel

/label ~"Marketing Programs" ~"status:wip" ~"MPM - Landing Page & Design"