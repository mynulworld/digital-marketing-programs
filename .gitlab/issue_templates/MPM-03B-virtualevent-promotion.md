## Purpose

This issue will track the high level tasks for cross-channel promotion (social,blog, and invitation emails) of virtual events.

## :computer: Website

* [ ] Add CTA to blog post (if applicable) - MPM

## 📧 Email

* [ ] Add CTA in bi-weekly newsletter - MPM
* [ ] Set up Marketo invitation email following process outlined in `MPM-03-invitations-reminder.md` - MPM
* [ ] Write copy for XDR invitation email - MPM
* [ ] Set up XDR invitation email in outreach - MPM
* [ ] Share XDR outreach invitation email template to XDRs/Sales in #sdrs_and_bdrs + #sales slack channels - MPM

## :bird: Social

* [ ] Create social issue for Emily VH to schedule promo tweets - MPM

Please submit any questions about this template to the #marketing-programs slack channel.

/assign @aoetama

/label ~"Marketing Programs" ~"status:wip" ~"MPM - Segmentation & Invitation"

