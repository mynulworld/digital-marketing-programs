☝️ **This issue is to be created by @jgragnola upon appproval (moved from WIP) meta event issue by Field Marketing, referred to as FMM throughout this issue.**

# EVENT DETAILS

* **FMM Owner:** FMM @
* **Type:** (select one) Conference, Owned Event, Field Event - [*see handbook for event type definitions*](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#Requesting-Marketing-Programs-Support-for-a-Field-Event)
* **Official Event Name:** 
* **Date:** (day of week, date)
* **Location:** 
* **Website:** 
* **Budgeted Cost:** `FMM fill in`

✅ **Types of Lead Lists we will Receive at Event:** 

* Booth Visitors
* Attendees
* No Shows
* Speaking Session
* Workshop
* Dinner
* Party

### [Copy Document for drafting, editing, translation, and final review before Marketo creation >>](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit#)

# ACTION ITEMS

## Facilitate Tracking of Campaigns in Systems

* [ ] Create Salesforce Campaign @jgragnola
  * [tbd link to salesforce campaign](https://na34.salesforce.com/701?fcf=00B61000004NY3B)
* [ ] Create Marketo Program @jgragnola
  * [tbd link to marketo program](https://app-ab13.marketo.com/#MF1521A1)
* [ ] Link MKTO <> SFDC @jgragnola
* [ ] Create Finance Tags @jgragnola (`MPM request from AP`)

## Create Confirmation Email (`1 month prior to event date`)

* [ ]  Write copy for "save the date" calendar event - FMM
* [ ]  Write copy for confirmation email - @jgragnola
* [ ]  Provide translation for "save the date" and confirmation email in Copy Document (international events) - TBD determined by FMM
* [ ]  Set up email in Marketo - @jgragnola
* [ ]  Test email - @jgragnola

## Create Registration Landing Page (`1 month prior to event date`)

* [ ]  Edit flows for each campaign - @jgragnola
* [ ]  Write copy for landing page - FMM
* [ ]  Check character limits in [tracking doc](https://docs.google.com/spreadsheets/u/1/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=868164112) - FMM
* [ ]  Provide edits in Copy Document -  @erica
* [ ]  Provide translation in Copy Document (international events) - TBD determined by FMM
* [ ]  Place code in www-gitlab-com & create MR - @jgragnola
* [ ]  Comment FMM into MR when pipeline is approved - @jgragnola
* [ ]  Review pipeline-approved page - FMM
* [ ]  Request merge from an MR approver - @jgragnola
* [ ]  Test fully functioning page live & notify FMM - @jgragnola

## Identify Target Segment (`5 BD before send date`)

* [ ]  Provide suggested filters and required capacity of event - FMM
* [ ]  Develop target list - @jgragnola
* [ ]  Review target list on call - @jgragnola + FMM

## Create Invitation Email (Write copy by: `insert copy due date` Send: `insert send date`)

* [ ]  Write copy for email - FMM
* [ ]  Provide edits in Copy Document -  @erica
* [ ]  Provide translation in Copy Document (international events) - TBD determined by FMM
* [ ]  Set up email in Marketo and edit copy as needed - @jgragnola
* [ ]  Set up calendar invite in Marketo MyTokens = @jgragnola
* [ ]  Test email & calendar invite - @jgragnola + FMM
* [ ]  Confirm in issue comments that email is set to send - @jgragnola
* [ ]  Confirm in issue comments that email went out - @jgragnola

## Create Reminder Email (Write copy by: `insert copy due date` Send: `insert send date`)

* [ ]  Write copy for email - @jgragnola
* [ ]  Set up email in Marketo - @jgragnola
* [ ]  Test email - @jgragnola & FMM
* [ ]  Confirm in issue comments that email is set to send - @jgragnola
* [ ]  Confirm in issue comments that email went out - @jgragnola

## Create Follow Up Email(s) in Marketo (Write copy by: `insert copy due date` Send: `insert send date`)

* [ ]  Write copy for email(s) - specify for attendees/no shows - FMM
* [ ]  Content Team provides edits in Copy Document
* [ ]  Set up email in Marketo and edit copy as needed - @jgragnola
* [ ]  Test email - @jgragnola & FMM
* [ ]  Confirm in issue comments that email is set to send - @jgragnola
* [ ]  Confirm in issue comments that email went out - @jgragnola

## Send List to MPM / MOps (`insert est. list receipt date`)

* [ ]  Remove landing page form, if a landing page exists for the event - @jgragnola
* [ ]  Cleanup list as necessary and designate with notes - FMM
* [ ]  Translate international lead lists as necessary - TBD determined by FMM
* [ ]  Link to ready-to-upload googledoc of list in issue comments and tag @jjcordz and @jgragnola - FMM
* [ ]  Upload list to SFDC - @jjcordz
* [ ]  Notify in issue when list is uploaded - @jjcordz
* [ ]  Lead list created for campaign and shared with XDRs - @jjcordz


Questions refer to [handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#Requesting-Marketing-Programs-Support-for-a-Field-Event)

fyi @jjcordz

/assign @jgragnola 

/label ~Events ~"Marketing Campaign" ~"Marketing Programs" ~"MktgOps-Priority::2 - Action Needed" ~"MPM - Facilitate Tracking"
