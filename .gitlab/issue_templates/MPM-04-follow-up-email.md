### 📝 [Epic Link >>](#)

## WIP email screenshot
**Sender: GitLab, info@**  
**Subject: fill in when final**

*copy paste image of most recent email here - when final, change the title of this section to "Final"*

## Purpose

This issue will track the identification for our target segment (should be included in prior reasoning for landing page creation), invitation and reminder emails for events. Adherence to schedules by supporting teams (field marketing and content) are required to hit deadlines for promotion in advance of event.

**Below to be completed by Marketing Program Manager with support of Field Marketing and Content Marketing.**

### [Copy Document >>](https://docs.google.com/document/u/1/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit) *make a copy of this template and provide requested copy*

**PLANNED SEND DATE:** `X` (Write copy by: `X`)

## 🗓 Add follow up email to [email marketing calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_bpjvmm7ertrrhmms3r7ojjrku0%40group.calendar.google.com&ctz=America%2FLos_Angeles)
* [ ]  Add follow up email to email marketing calendar

## 📧 Create and test email in Marketo
* [ ]  Write copy for email(s) - specify for attendees/no shows - campaign owner
* [ ]  Provide edits in Copy Document - MPM
* [ ]  Set up email in Marketo and edit copy as needed - MPM
* [ ]  Test email and send to owner for review - MPM
* [ ]  Test and approve email - campaign owner
* [ ]  Confirm in issue comments that email is set to send - MPM
* [ ]  Confirm in issue comments that email was sent out - MPM

Please submit any questions about this template to the #marketing-programs slack channel.

/label ~"Marketing Programs" ~"status:wip" ~"MPM - Reminders & Follow Up Emails"