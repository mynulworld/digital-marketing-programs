### 📝 [Epic Link >>](#)

## Purpose

This issue contains the retrospective document post virtual event.

### [Retro Document Template >>](https://docs.google.com/document/d/1rmqAYeJ0sUhDzAuF8TWlO2OKdfFhuY4l2tbJSAPv2KY/edit) *make a copy of this template and link it here.*

## Next steps

* [ ] Add relevant reports/dashboards & key insights to google document - MPM
* [ ] Ping all stakeholders involved in Virtual Event in issue comment asking them to fill out the doc - MPM
* [ ] Ping all stakeholders involved in Virtual Event in issue comment letting them know everyone has finished providing feedback - MPM

/assign @aoetama

/label ~"Marketing Programs" ~"status:wip" ~"MPM - Checks & Retrospective"