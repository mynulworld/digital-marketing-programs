   
[DRI](https://about.gitlab.com/handbook/people-operations/directly-responsible-individuals/) to fill in as much as possible and assign yourself as owner initially. Please ensure the interested parties are part of the business needs evaluation.

* **Name of tool**:
* **Evaluation DRI**:

### Evaluation process
* [ ]  Vendor pitch attended by:
* [ ]  Link to any documentation:
* [ ]  Buy-in from interested parties @ mention:
* [ ]  Demo
* [ ]  Any hands-on use
* [ ]  Label ~"MktgOPS \- Action Needed" label for technical evaluation
* [ ]  If approved beging [Vendor contract approval process](https://about.gitlab.com/handbook/finance/procure-to-pay/)

### Business needs:
*  What functions does this tool perform?  What business problem does it solve?

### Users:
* What departments/people benefit from its use and is there a representative from each dept to evaluate?

* How many seats/licenses will we need? Can we use a functional ID or will we need individual seats.


### Status of existing tool
* Do we currently have a tool in use for this function

* If we do, why are we replacing it?

* What the current contractual status? Link to copy of contract:


### New tool details
* How much does the tool cost? Is this per seat?

* Can we pilot/poc/trial this tool, or will we need to commit to a contract?

* How long is the contract?

* Is this budgeted? 

* What will this need to be integrated with? 

* Primary features of this tool
1.  
1.  
1.   

/label  ~"Tools&Tech" ~"MktgOPS \- FYI" 
